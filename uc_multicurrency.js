
if (Drupal.jsEnabled) {
  (function ($) {  // Put definition of $ into local scope so jQuery code
                   // doesn't conflict with other JavaScript libraries
  $(document).ready(function() {
    // Select the appropriate input based on radio button values
    // First time in...
    var j = $(".anonymous-radios").find("input:radio").eq(0).attr('checked');
    if (j) $('#edit-uc-multicurrency-price-message').attr('disabled', 'disabled');
    else $('#edit-uc-multicurrency-price-message').removeAttr('disabled');
 
    var k = $(".rounding-radios").find("input:radio").eq(0).attr('checked');
    if (k) $('#edit-uc-multicurrency-decimal-value').attr('disabled', 'disabled');
    else $('#edit-uc-multicurrency-decimal-value').removeAttr('disabled');
 
    // Subsequent clicks...
    $(".anonymous-radios").find("input:radio").click(function() {
      // As per Lyle, choosing to use click because of IE's stupid bug not to
      // trigger onChange until focus is lost.
      if ($(this).val() == 0) $('#edit-uc-multicurrency-price-message').attr('disabled', 'disabled');
      else $('#edit-uc-multicurrency-price-message').removeAttr('disabled');
    });
    $(".rounding-radios").find("input:radio").click(function() {
      if ($(this).val() == 0) $('#edit-uc-multicurrency-decimal-value').attr('disabled', 'disabled');
      else $('#edit-uc-multicurrency-decimal-value').removeAttr('disabled');
    });
  });

  }) (jQuery)  // End local scope for $
}
