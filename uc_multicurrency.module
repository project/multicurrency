<?php

/**
 * @file
 * Module to display Product prices in a localized currency.
 *
 * Prices should be entered into the database in the store's default
 * currency, but will be displayed to the user in a currency that is
 * determined by where the user is located.  Administration menus control
 * which locations see which currency.
 *
 * User location is taken from the $user object, where it is stored in
 * the country_iso_code_2 field ($user->country_iso_code_2).  You will
 * need another module to create this field and fill it with an appropriate
 * value!  One option is to use the core Drupal module "Profile" to allow
 * users to select their own country.  Another is to use the "ip2country"
 * module, which determines the user country based on IP address.
 *
 * @author Tim Rohaly.
 */


/******************************************************************************
 * Drupal Hooks                                                               *
 ******************************************************************************/


/**
 * Implementation of hook_help().
 */
function uc_multicurrency_help($path, $arg) {
  switch ($path) {
    case 'admin/help#uc_multicurrency':
    case 'admin/store/settings/multicurrency':
      return t("Ubercart module to display Product prices in localized currency, based on user's country.");
      break;
  }
}


/**
 * Implementation of hook_perm().
 */
function uc_multicurrency_perm() {
  return array('administer multicurrency');
}


/**
 * Implementation of hook_menu().
 *
 * Called when Drupal is building menus.  Cache parameter lets module know
 * if Drupal intends to cache menu or not - different results may be
 * returned for either case.
 *
 * @return
 *   An array with the menu path, callback, and parameters.
 */
function uc_multicurrency_menu() {
  $items = array();

  $items['admin/store/settings/multicurrency'] = array(
    'title'            => 'Multi-Currency Settings',
    'description'      => 'Conversion and Country-specific Settings',
    'access arguments' => array('administer multicurrency'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uc_multicurrency_defaults_form'),
    'type'             => MENU_NORMAL_ITEM,
  );
  $items['admin/store/settings/multicurrency/conversion'] = array(
    'title'            => 'Conversion Settings',
    'description'      => 'Currency Conversion Settings',
    'access arguments' => array('administer multicurrency'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uc_multicurrency_conversion_form'),
    'weight'           => 5,
    'type'             => MENU_LOCAL_TASK,
  );
  $items['admin/store/settings/multicurrency/countries'] = array(
    'title'            => 'Country Settings',
    'description'      => 'Country-specific Settings',
    'access arguments' => array('administer multicurrency'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uc_multicurrency_country_form'),
    'weight'           => 0,
    'type'             => MENU_LOCAL_TASK,
  );
  $items['admin/store/settings/multicurrency/currency'] = array(
    'title'            => 'Store Currencies',
    'description'      => 'Add or remove currencies',
    'access arguments' => array('administer multicurrency'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uc_multicurrency_currency_form'),
    'weight'           => -5,
    'type'             => MENU_LOCAL_TASK,
  );
  $items['admin/store/settings/multicurrency/defaults'] = array(
    'title'            => 'Default Values',
    'description'      => 'Set Default Values',
    'access arguments' => array('administer multicurrency'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uc_multicurrency_defaults_form'),
    'weight'           => -10,
    'type'             => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/store/settings/multicurrency/formats'] = array(
    'title'            => 'Format Settings',
    'description'      => 'Currency Format Settings',
    'access arguments' => array('administer multicurrency'),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uc_multicurrency_formats_form'),
    'weight'           => 10,
    'type'             => MENU_LOCAL_TASK,
  );
  $items['admin/store/settings/multicurrency/remove'] = array(
    'access arguments' => array('administer multicurrency'),
    'page callback'    => 'uc_multicurrency_currency_form_remove',
    'type'             => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implementation of hook_form_alter().
 */
function uc_multicurrency_form_alter(&$form, $form_state, $form_id) {
  // Add a new submit handler for the uc_store currency formats
  // settings form, so uc_multicurrency can be notified on change
  // of store default currency
  if ($form_id == 'uc_store_format_settings_form') {
    $form['#submit'][] = 'uc_store_format_settings_form_submit';
  }
}


/**
 * Implement the hook_theme() registry.
 */
function uc_multicurrency_theme() {
  return array(
    'uc_multicurrency_conversion_form' => array(
      'arguments' => array(
        'form' => NULL,
      ),
    ),
    'uc_multicurrency_formats_form' => array(
      'arguments' => array(
        'form' => NULL,
      ),
    ),
  );
}


/******************************************************************************
 * Menu Callbacks                                                             *
 ******************************************************************************/


/**
 * Submit handler for uc_store format settings.
 *
 * This routine adds a submit handler to a core uc_store form function.
 * In this submit handler, we make sure that when the store default currency
 * format is set, this module's currency format variables also get set.
 * Then we pass control back to the default submit handler so everything else
 * gets done.
 */
function uc_store_format_settings_form_submit($form, &$form_state) {

  $default_currency = variable_get('uc_currency_code', 'USD');
  variable_set('uc_currency_symbol_'. $default_currency,
               $form_state['values']['uc_currency_sign']);
  variable_set('uc_sign_after_amount_'. $default_currency,
               $form_state['values']['uc_sign_after_amount']);
  variable_set('uc_currency_thou_'. $default_currency,
               $form_state['values']['uc_currency_thou']);
  variable_set('uc_currency_dec_'. $default_currency,
               $form_state['values']['uc_currency_dec']);
  variable_set('uc_currency_prec_'. $default_currency,
               $form_state['values']['uc_currency_prec']);
}


/**
 * Form for selecting a currency to use for each supported country
 *
 * @return
 *   Forms for store administrator to set configuration options.
 */
function uc_multicurrency_country_form() {

  /* Container for currency selection preferences forms */
  $form['uc_multicurrency_currency_selection'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Country Currency Selections'),
    '#description'  => t('Select currency to sell in for each supported country.  Additional countries may be added via the !countries page.  Additional currencies may be added to the selection via the !currencies tab.', array('!countries' => l(t('Import countries'), 'admin/store/settings/countries/edit'), '!currencies' => l(t('Store Currencies'), 'admin/store/settings/multicurrency/currency'))),
    '#collapsible'  => FALSE,
    '#collapsed'    => FALSE,
  );

  // Loop over installed countries in uc_countries and present
  // a table with a currency selection box for each country.
  $result = db_query("SELECT * FROM {uc_countries} WHERE version > 0 ORDER BY country_name");
  while ($country = db_fetch_array($result)) {
    $form['uc_multicurrency_currency_selection']['uc_multicurrency_country_currency_'. $country['country_iso_code_2']] = uc_multicurrency_currency_select($country, FALSE);
  }

  return system_settings_form($form);
}


/**
 * Create a currency select box for a form.
 */
function uc_multicurrency_currency_select($country = NULL, $required = FALSE) {
  $currencies = _uc_multicurrency_currencies_in_use();

  $default_currency = variable_get('uc_currency_code', 'USD');

  $select = array(
    '#type'          => 'select',
    '#options'       => $currencies,
    '#default_value' => variable_get('uc_multicurrency_country_currency_'. $country['country_iso_code_2'], $default_currency),
    '#prefix'        => '<span>'. $country['country_name'] .'</span>',
    '#required'      => $required,
  );

  return $select;
}


/**
 * Create a country select box for a form.
 *
 * @param $display
 *   Can be 'name', 'code2' (the 2-digit code), or 'code3' (the 3-digit code).
 */
function uc_multicurrency_select($title, $default = NULL, $description = NULL, $display = 'name', $required = FALSE) {
  if ($display == 'name') {
    $order_by = 'country_name';
  }
  elseif ($display == 'code2') {
    $order_by = 'country_iso_code_2';
  }
  elseif ($display == 'code3') {
    $order_by = 'country_iso_code_3';
  }

  $result = db_query("SELECT * FROM {uc_countries} WHERE version > 0 ORDER BY '%s'", $order_by);

  $options = array();
  while ($country = db_fetch_array($result)) {
    $options[$country['country_id']] = $country[$order_by];
  }
  if (count($options) == 0) {
    $options[] = t('No countries found.');
  }

  $default = db_result(db_query("SELECT country_id FROM {uc_countries} WHERE country_id = %d AND version > 0", empty($default) ? 0 : intval($default)));

  $select = array(
    '#type'          => 'select',
    '#title'         => $title,
    '#description'   => $description,
    '#options'       => $options,
    '#default_value' => empty($default) ? uc_store_default_country() : $default,
    '#required'      => $required,
  );

  return $select;
}


/**
 * Form to allow selection of conversion preferences
 */
function uc_multicurrency_conversion_form() {
  $default_currency = variable_get('uc_currency_code', 'USD');

  // In-use currencies
  $in_use     = _uc_multicurrency_currencies_in_use();
  if (is_array($in_use)) {
    unset($in_use[$default_currency]);  // No need to configure USD -> USD conversions
    foreach ($in_use as $currency_code => $currency_name) {
      /* Container for conversion preferences forms */
      $form['uc_multicurrency_conversion_preferences_'. $currency_code] = array(
        '#type'         => 'fieldset',
        '#title'        => t('@money Conversion Preferences', array('@money' => $currency_name)),
        '#description'  => t('Set conversion factor and multiplier.  Market rate is @amount @money per @currency.', array('@amount' => uc_multicurrency_convert($default_currency, $currency_code, 1.0), '@money' => $currency_code, '@currency' => $default_currency)),
        '#collapsible'  => TRUE,
        '#collapsed'    => FALSE,
      );

      /* Form to set the conversion factor for each currency */
      $form['uc_multicurrency_conversion_preferences_'. $currency_code]['uc_multicurrency_factor_'. $currency_code] = array(
        '#type'          => 'textfield',
        '#title'         => t('Conversion Factor'),
        '#default_value' => variable_get('uc_multicurrency_factor_'. $currency_code, uc_multicurrency_convert($default_currency, $currency_code, 1.0)),
        '#description'   => t('Leave blank to use market rate currency conversion.  Entering a value here will override automatic rate determination.'),
        '#required'      => FALSE,
      );

      /* Form to set the multiplier for each currency */
      $form['uc_multicurrency_conversion_preferences_'. $currency_code]['uc_multicurrency_multiplier_'. $currency_code] = array(
        '#type'          => 'textfield',
        '#title'         => t('Multiplier'),
        '#default_value' => variable_get('uc_multicurrency_multiplier_'. $currency_code, 1.0),
        '#description'   => t('Mark up prices in this currency according to the multiplicative factor entered here.  Defaults to 1.0.'),
        '#required'      => TRUE,
      );
    }
  }

  // Override default theme function for system_settings_form
  $form = system_settings_form($form);
  $form['#theme'] = 'uc_multicurrency_conversion_form';

  return $form;
}


/**
 * Theme currency conversion form
 *
 * @ingroup themeable
 */
function theme_uc_multicurrency_conversion_form($form) {
  $default_currency = variable_get('uc_currency_code', 'USD');
  $output  = t("When creating products, currencies are assumed to be entered in the database in units of your store's default currency (@default). The price of a product in a different currency will be computed as follows:", array('@default' => $default_currency)) .'<br />';
  $output .= '<blockquote><pre>'. t('Price = @default * Factor * Multiplier', array('@default' => $default_currency)) .'</pre></blockquote>';
  $output .= t('There is one set of Factor/Multiplier for each supported currency.  Add or delete supported currencies at the !currencies tab.', array('!currencies' => l(t('Store Currencies'), 'admin/store/settings/multicurrency/currency')));
  $output .= drupal_render($form);

  return $output;
}


/**
 * Form to allow selection of store currencies
 */
function uc_multicurrency_currency_form() {
  // In-use currencies
  $in_use     = _uc_multicurrency_currencies_in_use();

  // Available currencies
  $currencies = currency_api_get_list();

  $header = array(t('Currency'), t('ISO Code'), t('Operations'));
  if (is_array($in_use)) {
    foreach ($in_use as $currency_code => $currency_name) {
      $row    = array($currency_name, $currency_code);
      $row[]  = l(t('remove'), 'admin/store/settings/multicurrency/remove/'. $currency_code);
      $rows[] = $row;
      // Remove in-use currency from list of available currencies
      unset($currencies[$currency_code]);
    }
  }

  // Define submit handler function
  $form['#submit'][] = 'uc_multicurrency_currency_form_submit';

  $form['text'] = array(
    '#type'     => 'markup',
    '#value' => t('The table below lists all the currencies used to sell products on your website.  To add a new currency, select it from the list of available currencies and click the "Add" button.'),
  );
  $form['import_'] = array(
    '#type'     => 'select',
    '#title'    => t('Available Currencies'),
    '#options'  => is_array($currencies) ? $currencies : array(t('-None available-')),
    '#disabled' => is_array($currencies) ? FALSE : TRUE,
  );
  $form['import_button'] = array(
    '#type'     => 'submit',
    '#value'    => t('Add'),
    '#disabled' => is_array($currencies) ? FALSE : TRUE,
  );
  $form['country_table'] = array(
    '#type'     => 'markup',
    '#value'    => theme('table', $header, $rows),
  );

  return $form;
}


/**
 * Submit handler for adding a currency to the list of available currencies
 */
function uc_multicurrency_currency_form_submit($form, &$form_state) {
  $in_use     = _uc_multicurrency_currencies_in_use();
  $currencies = currency_api_get_list();
  $in_use[$form_state['values']['import_']] = $currencies[$form_state['values']['import_']];
  variable_set('uc_multicurrency_currencies_in_use', $in_use);

  // Get the currency symbols
  $symbols = currency_api_get_symbols();

  // Set the currency symbol for the newly-added currency
  // All other currency-specific variables should default reasonably,
  // so the symbol is the only one we need to set explicitly
  variable_set('uc_currency_symbol_'. $form_state['values']['import_'],
               $symbols[$form_state['values']['import_']]);
}


/**
 * Menu Callback when a currency is removed from the list of available
 * currencies
 */
function uc_multicurrency_currency_form_remove($currency_code) {
  // Should we delete variables for the removed currency, or just
  // leave them so the configuration work isn't lost if that currency
  // gets re-enabled?

  $default_currency = variable_get('uc_currency_code', 'USD');

  $in_use = _uc_multicurrency_currencies_in_use();
  unset($in_use[$currency_code]);
  variable_set('uc_multicurrency_currencies_in_use', $in_use);

  // Fetch countries
  $result = db_query("SELECT * FROM {uc_countries} WHERE version > 0");
  while ($country = db_fetch_array($result)) {
    $currency_used = variable_get('uc_multicurrency_country_currency_'. $country['country_iso_code_2'], $default_currency);
    if ($currency_used == $currency_code) {
      variable_set('uc_multicurrency_country_currency_'. $country['country_iso_code_2'], $default_currency);
    }
  }

  drupal_goto('admin/store/settings/multicurrency/currency');
}


/**
 * Form for defining default values
 *
 * @return
 *   Forms for store administrator to set configuration options.
 */
function uc_multicurrency_defaults_form() {
  drupal_add_js(drupal_get_path('module', 'uc_multicurrency') .'/uc_multicurrency.js');

  /* Container for anonymous user settings */
  $form['uc_multicurrency_anonymous'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Anonymous User Rules'),
    '#collapsible'  => FALSE,
    '#collapsed'    => FALSE,
  );

  $form['uc_multicurrency_anonymous']['uc_multicurrency_anonymous_behavior'] = array(
    '#type'          => 'radios',
    '#title'         => t('If user is not logged on'),
    '#default_value' => variable_get('uc_multicurrency_anonymous_behavior', 0),
    '#attributes'    => array('class' => 'anonymous-radios'),
    '#options'       => array(
      t('Display Price for Default Country'),
      t('Display Message instead of Price'),
    ),
  );

  $form['uc_multicurrency_anonymous']['uc_multicurrency_price_message'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Message'),
    '#default_value' => variable_get('uc_multicurrency_price_message', t('Please log in to see prices')),
    '#description'   => t('Message to display to anonymous users instead of price.'),
    '#required'      => FALSE,
  );

  /* Container for default country setting */
  $form['uc_multicurrency_country_defaults'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Default Country Setting'),
    '#collapsible'  => FALSE,
    '#collapsed'    => FALSE,
  );

  $dd = variable_get('uc_multicurrency_default_country', 'US');
  $form['uc_multicurrency_country_defaults']['uc_multicurrency_default_country'] = uc_country_select(t('Default Country'), $dd, t("Used when a country can't be determined, or when user isn't logged in."), 'name', FALSE);

  /* Container for rounding preferences forms */
  $form['uc_multicurrency_rounding'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Price Rounding Rules'),
    '#description'  => t('Prices will be modified following these rules after the currency conversion is performed:'),
    '#collapsible'  => FALSE,
    '#collapsed'    => FALSE,
  );

  $form['uc_multicurrency_rounding']['uc_multicurrency_rounding'] = array(
    '#type'          => 'radios',
    '#title'         => t('Price Rounding'),
    '#default_value' => variable_get('uc_multicurrency_rounding', 0),
    '#attributes'    => array('class' => 'rounding-radios'),
    '#options'       => array(
      t('No Rounding'),
      t('Round UP to nearest currency unit'),
      t('Round DOWN to nearest currency unit'),
    ),
  );

  /* Form to set the decimal value of the price, if specified */
  $form['uc_multicurrency_rounding']['uc_multicurrency_decimal_value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Decimal value'),
    '#description'   => t("Enter a two-digit number. When rounding, the decimal part of all prices will be set to this two-digit number. If left blank AND if rounding was chosen above, decimal value won't be displayed."),
    '#default_value' => variable_get('uc_multicurrency_decimal_value', 0),
  );

  return system_settings_form($form);
}


/**
 * Validation function for default values form
 */
function uc_multicurrency_defaults_form_validate($form, &$form_state) {
  $value = $form_state['values']['uc_multicurrency_decimal_value'];
  if (value == '') {
    return;
  }

  if ($value < 0 || $value > 99) {
    form_set_error('uc_multicurrency_decimal_value',
                   t('Decimal value must be a positive number less than 100'));
  }
}


/**
 * Form to allow selection of currency format preferences
 */
function uc_multicurrency_formats_form() {
  $default_currency = variable_get('uc_currency_code', 'USD');

  // Get the currency symbols
  $symbols = currency_api_get_symbols();

  // In-use currencies
  $in_use  = _uc_multicurrency_currencies_in_use();
  if (is_array($in_use)) {

    // The default currency is set at admin/store/settings/store/edit/formats
    // Here, we make sure this module's values for the default currency
    // are set the same as the store's
    variable_set('uc_currency_symbol_'. $default_currency,
                 variable_get('uc_currency_sign', '$'));
    variable_set('uc_sign_after_amount_'. $default_currency,
                 variable_get('uc_sign_after_amount', FALSE));
    variable_set('uc_currency_thou_'. $default_currency,
                 variable_get('uc_currency_thou', ','));
    variable_set('uc_currency_dec_'. $default_currency,
                 variable_get('uc_currency_dec', '.'));
    variable_set('uc_currency_prec_'. $default_currency,
                 variable_get('uc_currency_prec', 2));

    // Don't allow store defaults to be changed here
    unset($in_use[$default_currency]);

    foreach ($in_use as $currency_code => $currency_name) {

      /* Container for formats settings forms */
      $form['uc_multicurrency_formats_'. $currency_code] = array(
        '#type'        => 'fieldset',
        '#title'       => t('@money Format Preferences', array('@money' => $currency_name)),
        '#description' => t('Set conversion factor and multiplier.'),
        '#collapsible' => TRUE,
        '#collapsed'   => FALSE,
      );

      /* Form to display how this currency will be formatted */
      $form['uc_multicurrency_formats_'. $currency_code]['uc_multicurrency_format_'. $currency_code] = array(
        '#type'        => 'textfield',
        '#title'       => t('Current format'),
        '#value'       => uc_currency_format(1000.1234, TRUE, TRUE, NULL, $currency_code),
        '#description' => t('Prices in this currency will display as shown above.'),
        '#disabled'    => TRUE,
        '#size'        => 10,
      );

      /* Form to set the currency symbol for each currency */
      $form['uc_multicurrency_formats_'. $currency_code]['uc_currency_symbol_'. $currency_code] = uc_textfield(t('Currency Symbol'), variable_get('uc_currency_symbol_'. $currency_code, $symbols[$currency_code]), TRUE, NULL, 10, 10);

      /* Form to select whether currency symbol is shown before or after */
      /* the numerical amount for each currency                          */
      $form['uc_multicurrency_formats_'. $currency_code]['uc_sign_after_amount_'. $currency_code] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Display currency sign after amount.'),
        '#default_value' => variable_get('uc_sign_after_amount_'. $currency_code, FALSE),
      );

      /* Form to set the thousands separator for each currency */
      $form['uc_multicurrency_formats_'. $currency_code]['uc_currency_thou_'. $currency_code] = uc_textfield(t('Thousands Marker'), variable_get('uc_currency_thou_'. $currency_code, ','), FALSE, t("You may leave this field blank if you don't want a Thousands marker.  You may use a space as a marker if desired."), 10, 10);

      /* Form to set the decimal separator for each currency */
      $form['uc_multicurrency_formats_'. $currency_code]['uc_currency_dec_'. $currency_code] = uc_textfield(t('Decimal Marker'), variable_get('uc_currency_dec_'. $currency_code, '.'), TRUE, NULL, 10, 10);

      /* Form to set the number of decimal digits for each currency */
      $form['uc_multicurrency_formats_'. $currency_code]['uc_currency_prec_'. $currency_code] = array(
        '#type'          => 'select',
        '#title'         => t('Number of decimal places'),
        '#options'       => drupal_map_assoc(array(0, 1, 2)),
        '#default_value' => variable_get('uc_currency_prec_'. $currency_code, 2),
        '#required'      => TRUE,
      );
    }
  }

  // Override default theme function for system_settings_form
  $form = system_settings_form($form);
  $form['#theme'] = 'uc_multicurrency_formats_form';

  return $form;
}


/**
 * Theme currency conversion form
 *
 * @ingroup themeable
 */
function theme_uc_multicurrency_formats_form($form) {
  $default_currency = variable_get('uc_currency_code', 'USD');
  $output  = t("Use this form to control how a price is displayed in each of the store's currencies.  ");
  $output .= t('Add or delete supported currencies at the !currencies tab. ', array('!currencies' => l(t('Store Currencies'), 'admin/store/settings/multicurrency/currency')));
  $output .= t("Formats for your default store's default currency (@default) may be set on the !formats page. ", array('@default' => $default_currency, '!formats' => l(t('Store Formats'), 'admin/store/settings/store/edit/format')));
  $output .= drupal_render($form);

  return $output;
}


/******************************************************************************
 * Blocks                                                                     *
 ******************************************************************************/


/**
 * Implementation of hook_block().
 *
 * Provides an EXAMPLE of how to allow a user to manipulate the
 * $user->country_iso_code_2 value.
 */
function uc_multicurrency_block($op = 'list', $delta= 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Country Selection');
      return $blocks;

    case 'view':
      $block['subject'] = t('Country Selection');
      $block['content'] = drupal_get_form('uc_multicurrency_country_select_form');
      return $block;
  }
}


/**
 * Country selection form
 */
function uc_multicurrency_country_select_form(&$form_state) {
  global $user;

  // Define submit handler function
  $form['#submit'][] = 'uc_multicurrency_country_select_form_submit';

  $current_selection = uc_get_country_data(array('country_iso_code_2' => uc_multicurrency_get_country()));

  $form['country_selection'] = uc_country_select(NULL, $current_selection[0]['country_id'], NULL, 'name', FALSE);
  $form['select_button'] = array(
    '#type'     => 'submit',
    '#value'    => t('Apply'),
  );
  return $form;
}


/**
 * Submit handler for country selection form
 */
function uc_multicurrency_country_select_form_submit($form, &$form_state) {
  $result = db_result(db_query("SELECT country_iso_code_2 FROM {uc_countries} WHERE country_id = %d", $form_state['values']['country_selection']));
  uc_multicurrency_set_country($result);
}


/******************************************************************************
 * Ubercart Hooks                                                             *
 ******************************************************************************/


/**
 * Implementation of hook_order().
 *
 * Attach currency information to $order object.  All information necessary
 * to recreate the currency conversion at some time in the future is stored,
 * or all information needed to recreate a currency conversion that took place
 * in the past is retrieved.
 */
function uc_multicurrency_order($op, &$order, $arg2) {

  switch ($op) {
    case 'new':
      // Add currency data to $order object

      $store_currency   = variable_get('uc_currency_code', 'USD');
      $country          = uc_multicurrency_get_country();
      $sale_currency    = variable_get('uc_multicurrency_country_currency_'. $country, $store_currency);
      $rounding         = variable_get('uc_multicurrency_rounding', 0);
      $rounding_decimal = (int) variable_get('uc_multicurrency_decimal_value', 0);

      $order->currency                          = array();
      $order->currency['store_currency']        = $store_currency;
      $order->currency['sale_currency']         = $sale_currency;
      $order->currency['conversion_factor']     = (float) uc_multicurrency_get_factor($store_currency, $sale_currency);
      $order->currency['conversion_multiplier'] = (float) uc_multicurrency_get_multiplier($sale_currency);
      $order->currency['rounding']              = $rounding;
      $order->currency['rounding_decimal']      = $rounding_decimal;
      break;

    case 'load':
      // Load currency conversion information for this order
      $result = db_query("SELECT * FROM {uc_order_currency} WHERE order_id = %d", $order->order_id);
      if ($data = db_fetch_object($result)) {
        $order->currency                          = array();
        $order->currency['store_currency']        = $data->store_currency;
        $order->currency['sale_currency']         = $data->sale_currency;
        $order->currency['conversion_factor']     = $data->conversion_factor;
        $order->currency['conversion_multiplier'] = $data->conversion_multiplier;
        $order->currency['rounding']              = $data->rounding;
        $order->currency['rounding_decimal']      = $data->rounding_decimal;
      }
      else {
        // No currency data for this order.
        // Assume store currency = sale currency and multiplier = 1.0
        $store_currency = variable_get('uc_currency_code', 'USD');

        $order->currency                          = array();
        $order->currency['store_currency']        = $store_currency;
        $order->currency['sale_currency']         = $store_currency;
        $order->currency['conversion_factor']     = 1.0;
        $order->currency['conversion_multiplier'] = 1.0;
        $order->currency['rounding']              = 0;  // No rounding
        $order->currency['rounding_decimal']      = 0;
      }
      break;

    case 'save':
      // Save currency conversion information
      db_query("UPDATE {uc_order_currency} SET store_currency = '%s', sale_currency = '%s', conversion_factor = %f, conversion_multiplier = %f, rounding = %d, rounding_decimal = %d WHERE order_id = %d",
               $order->currency['store_currency'],
               $order->currency['sale_currency'],
               $order->currency['conversion_factor'],
               $order->currency['conversion_multiplier'],
               $order->currency['rounding'],
               $order->currency['rounding_decimal'],
               $order->order_id
      );
      if (db_affected_rows() == 0) {
        db_query("INSERT INTO {uc_order_currency} (order_id, store_currency, sale_currency, conversion_factor, conversion_multiplier, rounding, rounding_decimal) VALUES (%d, '%s', '%s', %f, %f, %d, %d)",
                 $order->order_id,
                 $order->currency['store_currency'],
                 $order->currency['sale_currency'],
                 $order->currency['conversion_factor'],
                 $order->currency['conversion_multiplier'],
                 $order->currency['rounding'],
                 $order->currency['rounding_decimal']
        );
      }
      break;

    case 'delete':
      // Remove currency conversion data associated with this order
      db_query("DELETE FROM {uc_order_currency} WHERE order_id = %d", $order->order_id);
      break;
  }
}


/******************************************************************************
 * Module Functions                                                           *
 ******************************************************************************/


/**
 * Get the country code from the $user object.  Returns default country
 * if no country set in $user object.
 *
 * @return
 *   ISO 3166 2-character country code
 */
function uc_multicurrency_get_country() {
  global $user;
  $country = $user->country_iso_code_2;

  if (!$country) {
    // No country in $user object - fallback to module default country
    $country = db_result(db_query("SELECT country_iso_code_2 FROM {uc_countries} WHERE country_id = %d", variable_get('uc_multicurrency_default_country', '840')));
  }

  return $country;
}


/**
 * Sets the country code in the $user object.
 *
 * Works only for logged-in users.
 *
 * @param $country
 *   ISO 3166 2-character country code
 */
function uc_multicurrency_set_country($country) {
  global $user;

  if ($user->uid) {  // Only for logged-in users
    user_save($user, array('country_iso_code_2' => $country));
  }
}


/**
 * Convenience method to return just one element of the conversion array
 *
 * @param $from
 *   ISO 4217 3-character currency code to convert from
 * @param $to
 *   ISO 4217 3-character currency code to convert to
 * @param $quantity
 *   Floating-point number - quantity of currency to convert
 */
function uc_multicurrency_convert($from, $to, $quantity) {
  $value = currency_api_convert($from, $to, $quantity);

  return $value['value'];
}


/**
 * Convenience routine for currency selection
 *
 * Returns an array of currencies that the store uses to display prices.
 * These are the only currencies that are used in the store.
 *
 * The currencies available on a fresh installation are set in
 * hook_install(); by default only U.S. Dollars (USD) and
 * Canadian Dollars (CAD) are available.
 */
function _uc_multicurrency_currencies_in_use() {
  // In-use currencies
  return variable_get('uc_multicurrency_currencies_in_use',
                       array(
                         'USD' => t('U.S. Dollar (USD)'),
                         'CAD' => t('Canadian Dollar (CAD)'),
                       )
                     );
}


/**
 * Determines the conversion factor between the store's default currency
 * and the currency passed in as the first argument.
 *
 * @param $default_currency
 *   ISO 4217 3-character currency code
 *
 * @param $currency
 *   ISO 4217 3-character currency code
 *
 * @return
 *   Conversion factor for the chosen currency
 */
function uc_multicurrency_get_factor($default_currency, $currency) {
  // Check to see if $factor is hardcoded from the admin menu
  $factor = variable_get('uc_multicurrency_factor_'. $currency,
                         uc_multicurrency_convert($default_currency, $currency, 1.0));
  if ($factor == '') {
    // If not, use market rate
    $factor = uc_multicurrency_convert($default_currency, $currency, 1.0);
  }

  return $factor;
}


/**
 * Determines the conversion multiplier for the currency passed in as
 * the first argument.
 *
 * @param $currency
 *   ISO 4217 3-character currency code
 *
 * @return
 *   Conversion multiplier for the chosen currency
 */
function uc_multicurrency_get_multiplier($currency) {
  // Get the multiplier for the chosen currency
  $multiplier = variable_get('uc_multicurrency_multiplier_'. $currency, 1.0);

  return $multiplier;
}


/**
 * Format an amount for display with the store's currency settings.
 *
 * This is a modified version of the core Ubercart function uc_currency_format()
 * The core function must be renamed so that this one can be used.
 */
function uc_currency_format($value, $sign = TRUE, $thou = TRUE, $dec = NULL, $currency_code = NULL) {

  global $user;

  // Decide how to format prices for anonymous users
  if (!$user->uid) {  // Anonymous
    if (variable_get('uc_multicurrency_anonymous_behavior', 0)) {
      return variable_get('uc_multicurrency_price_message',
                          t('Please log in to see prices'));
    }
  }

  // Get the chosen currency for this country
  $default_currency = variable_get('uc_currency_code', 'USD');
  if (!empty($currency_code)) {
    // Argument passed in - use it!
    $currency = $currency_code;
  }
  else {
    // No argument passed in, figure out what currency to use

    // Get the country to use
    if (!$user->uid) { // Anonymous
      $country = variable_get('uc_multicurrency_default_country', 'US');
    }
    else {
      $country  = uc_multicurrency_get_country();
    }
    $currency = variable_get('uc_multicurrency_country_currency_'. $country, $default_currency);
  }

  // Get the currency symbol for this currency
  $symbol = variable_get('uc_currency_symbol_'. $currency,
                         variable_get('uc_currency_sign', '$'));

  // Get the conversion factor for the chosen currency
  $factor = (float) uc_multicurrency_get_factor($default_currency, $currency);

  // Get the multiplier for the chosen currency
  $multiplier = (float) uc_multicurrency_get_multiplier($currency);

  // Change the price according to the conversion factor and the
  // multiplier for the chosen currency.
  $value = (float) $value * $multiplier * $factor;

  // Set decimal value to $fraction if rounding
  $fraction = (float) variable_get('uc_multicurrency_decimal_value', 0);

  // Perform rounding if desired.
  // NEVER ROUND FOR DEFAULT CURRENCY!  NEVER ROUND ZERO VALUES!
  // If decimal value is left blank AND rounding is enabled, suppress
  // display of decimal fraction
  if ($country != variable_get('uc_multicurrency_default_country', 'US') && $value != 0) {
    $suppress = FALSE;
    switch (variable_get('uc_multicurrency_rounding', 0)) {
      default:
      case 0:
        // No rounding
        break;

      case 1:
        // Round UP
        if ($fraction=='') $suppress = TRUE;
        $value = (float) ceil($value) + $fraction / 100.0;
        break;

      case 2:
        // Round DOWN
        if ($fraction=='') $suppress = TRUE;
        $value = (float) floor($value) + $fraction / 100.0;
        break;
    }
  }

  $default_sign_after = variable_get('uc_sign_after_amount', FALSE);
  $default_thou       = variable_get('uc_currency_thou', ',');
  $default_dec        = variable_get('uc_currency_dec', '.');
  $default_prec       = variable_get('uc_currency_prec', 2);

  if (variable_get('uc_currency_prec_'. $currency, $default_prec) > 0) {
    if (abs($value) < '.'. str_repeat('0', variable_get('uc_currency_prec_'. $currency, $default_prec) - 1) .'1') {
      $value = 0;
    }
  }

  if ($value < 0) {
    $value = abs($value);
    $format = '-';
  }

  if ($sign && !variable_get('uc_sign_after_amount_'. $currency, $default_sign_after)) {
    $format .= $symbol;
  }

  $format .= number_format(
    $value,
    $suppress ? 0 : variable_get('uc_currency_prec_'. $currency, $default_prec),
    !is_null($dec) ? $dec : variable_get('uc_currency_dec_'. $currency, $default_dec),
    $thou ? variable_get('uc_currency_thou_'. $currency, $default_thou) : ''
  );

  if ($sign && variable_get('uc_sign_after_amount_'. $currency, $default_sign_after)) {
    $format .= $symbol;
  }

  return $format;
}
