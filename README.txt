About
=====
This module enhances Ubercart by allowing a store to display product prices to
customers in multiple currencies.  The currency to display is chosen based
on the geographical location (country) of the customer, along with some
administration settings.  Knowing the country, the module will display prices
in a currency appropriate for that country.

Many configuration options are available to choose which currencies are used
to sell products, and which currency should be displayed to which country.
Conversion rates can be determined dynamically via a web service or hardwired
by the administrator.  Display format can be set separately for each currency.
Converted currency prices may be rounded if desired. 

Determination of the user's country is separated from this module for more
flexibility.  You may use the ip2country module to automatically select
a country based on the user's IP address, or you may use the Profile module,
for example, to collect the user's country on the registration page. Or you
can present a choice at the top of the page which the user can select to
pick his/her country.  Or you can link this choice to internationalization and
language selection.  In short, the multi-currency functionality requires only
that the country is set in the $user object - how it gets there is of no
concern.



Installation
============
Check requirements:  PHP 5 with cURL support enabled.
Drupal 5.7+ (Drupal 6 not allowed), Ubercart 1.0 RC5 or above.

Before you start, make sure your store's default currency is properly set.
Your store's default currency is shown at admin/store/settings/store/formats
There is a problem in Ubercart 1.0 where this can't be set by the user
interface, although it worked in previous versions of Ubercart and is fixed
for subsequent versions.  If you don't see an option to change the default
currency at admin/store/settings/store/edit/format you will need to insert
the proper value directly into the database using the following SQL statement
in phpMyAdmin or similar tool:
    INSERT INTO variable (`name`, `value`)  VALUES ('uc_currency_code',
's:3:"USD";')
where you should substitute your desired ISO 4217 3-letter currency code for
USD in the above statement.

This module depends on the Currency API module (http://drupal.org/project/currency).
Copy both these modules into your sites/all/modules directory and unzip/untar
them.

In your web browser, navigate to admin/build/modules and enable the following
modules (but don't press the "Submit" button yet!):

Currency API
Multi-Currency

Before you press the "Submit" button to enable these modules, edit
sites/all/modules/ubercart/uc_store/uc_store.module and rename the
uc_currency_format() function - this function is redefined in
uc_multicurrency.module and having two functions with the same name is
not allowed.  It doesn't matter what you rename it to - I use the name
uc_currency_format_original() on my site to let me know it's the unmodified
version of the function.

NOW you can press "Submit" to enable the modules.

(NOTE:  When un-installing this module, you will have to reverse the
procedure. After un-checking the Multi-Currency module at admin/buid/modules,
restore the function name in uc_store.module then press "Submit" to uninstall.)
 
This module defines a permission called "administer multicurrency", which
must be explicitly enabled for the administration user at admin/user/access.

You should now enter values in the administration menus.  Defaults are chosen
reasonably, but you should examine them and set them as you wish.

Go to admin/settings/currency_api to review and change settings for the
Currency API.  I recommend turning OFF the logging of currency exchange
requests (or your logs will quickly become swamped with these messages).
Choose a frequency to update the currency conversion factors.  Remember,
when the conversion factors change, your users will see a changed price,
so you might consider setting the frequency to once a day or even once a
week.

Go to admin/store/settings/multicurrency to review and change settings
Multi-Currency module.  There are many options here, in five separate tabs.
Select the tabs one at a time, carefully read the information, and choose
values appropriate for your business.

Everything should now work.  If it doesn't, read the rest of this document
(which you really should have done first, anyway!).  If you still have
problems see the "Troubleshooting" section below.



Features
========
This module displays Product prices in a localized currency.  Product prices
are entered into the database in your store's default currency, but are
displayed to the user in a currency determined by where the user is located.
Administration menus control the conversion factors, which selling currencies
are available, and which locations see which currency.

User location is taken from the $user object, where it is stored in the
country_iso_code_2 field ($user->country_iso_code_2).  You will need another
module to create this field and fill it with an appropriate value!  One
option is to use the core Drupal module "Profile" to allow users to select
their own country.  Another is to use the accompanying "ip2country" module,
which determines the user country based on IP address.  If this field doesn't
exist, or a country hasn't been determined for the user, uc_multicurrency
has an administration option to set the default country to use.  (A "Country
Selection" block is included with this module as an example of how to provide
a user interface element to allow your customers to select their own country.)

The Currency API module is used to look up currency conversion factors.
Currency conversion factors are taken from Yahoo! Finance on a periodic basis,
as set in the menu at admin/settings/currency_api.  These conversion factors
are stored in the database for the entire period, to minimize traffic and page
load times.  By default, currencies will be converted at their market rate.
You may override this at admin/store/settings/multicurrency/conversion
A multiplicative factor may also be set for each selling currency, to allow
the store to markup prices for sales in that currency.



Requirements
============
This module is known to work with Drupal 5.x and all Ubercart releases greater
than 1.0 RC5.  While it may run in older versions, I won't promise anything.
Future version of this module WILL be backwards compatible with this initial
release.

This module depends on Currency Exchange (http://drupal.org/project/currency)
version 5.x-1.1 or greater.  Earlier versions will not work properly with this
module.



Limitations
===========
Because Drupal caches pages for anonymous users, the only way to handle
displaying multiple currencies to different anonymous users is to turn off
caching entirely (which I don't recommend).  The alternative is to code the
module as I have done, to allow administrators a choice of behaviors.  The
administrator can configure the store in two ways:
  1) Anonymous users can't see prices - only logged-on users can see prices.
Anonymous users are shown a configurable message "Please log in to see prices"
anywhere a price would normally appear.
  2) Anonymous users see prices using the default store currency.

This module requires a modification to the core Ubercart function
uc_currency_format().  Addressing the multi-currency issue in this function
is the cleanest way to deal with the problem, minimizes modifications to the
Ubercart core, and allows these modifications to affect *all* other Ubercart
core and contributed modules which use product prices.  As a result, shipping,
taxes, etc. just work, as should any contributed modules.

I have not tested this module with many payment gateways.  Depending on how
the gateway was written, it may or may not do the right thing.  I am assuming
the gateway will take the order total and send it to the payment processor in
the store's default currency.  If you desire to actually charge the customer
in a different currency, you will have to ensure that the converted value
(as output by uc_currency_format()) is sent, and that information identifying
the currency to use for the transaction is sent.



Troubleshooting
===============
Does your site have PHP 5 built with cURL support?  Execute <?php phpinfo();?>
to see the details of your PHP installation.

You must have Ubercart and the Currency API modules installed and enabled.

Countries must have installed .cif files - if your customers can't enter
their country during the checkout, you can't sell there!  Go to
admin/store/settings/countries/edit to import .cif files for the countries
you want to sell to.

As described above, this module does not determine the user's country by
itself - you need a separate mechanism to fill in this information.  A
"Country Selection" block is provided as an EXAMPLE of how to allow a user
to manipulate the $user->country_iso_code_2 value.  With this block, any
user can dynamically change his country - this is very useful when testing
how your site will appear to other users.  You may enable this block at
admin/build/block.  I suggest configuring the block to be visible only for
logged-in users.

When all else fails, read the comments in the code - there are some debugging
print statements left in that can be uncommented to see what is going on, and
most functions are described in the comments.
